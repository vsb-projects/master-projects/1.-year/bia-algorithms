const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    // entry: ['./src/js/AlgorithmsWith3Dgraph.js', './src/js/permutations.js', './src/js/GeneticsAlgorithm.js'],
    // entry: ['./src/js/GeneticsAlgorithm.js'],
    // entry: ['./src/js/DifferentialEvolutionAlgorithm.js'],
    // entry: ['./src/js/SOMA.js'],
    // entry: ['./src/js/ParticleSwarm.js'],
    // entry: ['./src/js/AntColony.js'],
    // entry: ['./src/js/Firefly.js'],
    entry: ['./src/js/TLBO.js'],
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist'),
    },
    devServer: {
        contentBase: './dist'
    },
    plugins: [
        new HtmlWebpackPlugin ({
            filename: 'index.html',
            template: './src/index.html'
        })
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'ify-loader'
            }
        ]
    },

};