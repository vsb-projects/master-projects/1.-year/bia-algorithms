import {GriewankFunc, SphereFunc} from "./utils/functions";
import {animate, generateGraphFromFunction, getData, GraphType, render} from "./utils/renderGraph";
import {ParticlePoint, Point3D, DimensionalPoint} from "./models/Point";
import {randomFromInterval} from "./utils/utilFunctions";



const sortPoints = () => {
    return fireflyPoints.sort((a,b) => {
        if ( a.z < b.z ){
            return -1;
        }
        if ( a.z > b.z ){
            return 1;
        }
        return 0;
    });
};


const calculateEuclidianDistance = (fireflyPoint1, fireflyPoint2) => {
    let suma = 0.0;
    for(let i = 0; i < fireflyPoint1.d.length; i++){
        suma += Math.pow(fireflyPoint1.d[i] - fireflyPoint2.d[i],2);
    }
    return Math.sqrt(suma);
};

const calculateAtractivness = (r) => {
  return beta * Math.pow(Math.E,((-gamma)*Math.pow(r,m)));
};

const calculateAtractivnessNew = (r) => {
    const psi = 1.0;
    return beta * ((1)/(psi + r));
};

const calculateMovement = (fireflyBetter, fireflyWorse, func) => {
    let r = calculateEuclidianDistance(fireflyBetter, fireflyWorse);

    for(let i = 0; i < fireflyWorse.d.length; i++){
        fireflyWorse.d[i] = fireflyWorse.d[i] + calculateAtractivnessNew(r) * (fireflyBetter.d[i] - fireflyWorse.d[i]) + alpha * (Math.random() - 0.5);
    }

    // Calculate new fitness
    fireflyWorse.valuateSelf(func);
};

const calculateMovementForTheBest = (fireflyBest, func) => {
    for(let i = 0; i < fireflyBest.d.length; i++){
        fireflyBest.d[i] += alpha * (Math.random() - 0.5);
    }
    fireflyBest.valuateSelf(func);
};


/**
 * Firefly algorithm
 */
const ITERATIONS = 50;
// const range = 20.0;
const range = 5.0;
const population = 10;

// CONSTANTS
const m = 1;
const alpha = 0.3; // DO NOT CHANGE
const beta = 1.9; // DO NOT CHANGE
const gamma = 0.5;


// Searching fireflyPoints
const fireflyPoints = [];


const FireflyAlgorithm = (func, dimensions) => {
    // Generate particlePoints
    for(let i = 0; i < population;i++){
        let pointsArray = [];
        for(let j = 0; j < dimensions; j++){
            pointsArray.push(randomFromInterval(-range,range));
        }

        fireflyPoints.push(new DimensionalPoint(pointsArray, func(pointsArray)));
    }

    console.log("Init fireflyPoints", fireflyPoints);

    sortPoints();

    animate(fireflyPoints);

    for(let i = 0; i < ITERATIONS; i++) {

        for(let j = 1; j < population; j++){
            for(let k = 1; k < population; k++){
                if(fireflyPoints[j].z < fireflyPoints[k].z){
                    calculateMovement(fireflyPoints[j], fireflyPoints[k], func);
                }
            }
        }
        calculateMovementForTheBest(fireflyPoints[0], func);
        sortPoints();

        animate(fireflyPoints);
    }

    sortPoints();

    animate(fireflyPoints);

    console.log(fireflyPoints);
    console.log("GLOBAL MINIMUM", fireflyPoints[0].z);
};

const START = (func, graphType) => {
    render(getData(generateGraphFromFunction(range, func), graphType));
    FireflyAlgorithm(func,5);
};

START(GriewankFunc, GraphType.MESH);
// START(SphereFunc, GraphType.SCATTER);