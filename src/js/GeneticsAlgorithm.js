import Plotly from 'plotly.js-dist';
import Trace from "./models/Trace";
import Route from "./models/Route";
import City from "./models/City";
import { copyObject, Create2DArray, random_normal } from "./utils/utilFunctions";

let CANVAS = document.querySelector('#canvas');
let input = document.getElementById('in');

export function shuffleArray(currentArray, shuffleTimes, routes, startCity, endCity) {
    for(let k = 0; k < shuffleTimes;k++) {
        const array = [...currentArray];
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }

        // array.unshift(startCity);
        // array.push(endCity);

        const route = new Route(array);
        route.calculateDistance();
        routes.push(route);
    }
}


/**
 * Genetics Algorithm
 */
const GENERATIONS = 2000;
let numberOfCities = 10;
const numberOfGeneratedRoutes = 10;
const PointsRange = 100;
const MR = 0.5;
const CR = 0.1;

// Routes
export let routes = [];

const cityArray = [];
export const distancesMatrix = Create2DArray(numberOfCities);

const calculateFitness = () => {
    routes.sort((a,b) => {
            if ( a.distance < b.distance ){
                return -1;
            }
            if ( a.distance > b.distance ){
                return 1;
            }
            return 0;
    });
    for(let i = 0; i < routes.length; i++){
        routes[i].fitness = i;
    }
    // console.log("Fitness routes", routes);
};

const selection = (firstParentIndex) => {
    let secondParentIndex = random_normal(0,routes.length-1);

    while(firstParentIndex === secondParentIndex){
        secondParentIndex = random_normal(0,routes.length-1);
    }
    return [routes.splice(firstParentIndex,1).pop(), routes[secondParentIndex]];
};

const crossover = (firstParent, secondParent) => {
    let size = firstParent.cities.length;
    let randomLeftIndex = random_normal(0,Math.floor(size/2));
    let randomRightIndex = random_normal(Math.floor(size/2),size-1);

    let child = [];

    for(let i = 0;i < randomLeftIndex;i++){
        child.push(firstParent.cities[i]);
    }

    for(let i = randomRightIndex; i < size;i++){
        child.push(firstParent.cities[i]);
    }

    for(let i = randomLeftIndex;i < randomRightIndex;i++){
        if(!child.includes(secondParent.cities[i])) {
            child.splice(i,0,secondParent.cities[i]);
        }else if (!child.includes(firstParent.cities[i])){
            child.splice(i,0,firstParent.cities[i]);
        }
    }

    for(let i = 0; i < firstParent.cities.length; i++){
        if(!child.includes(firstParent.cities[i])){
            child.splice(randomRightIndex,0,firstParent.cities[i]);
        }
    }
    // child.filter(item => item);

    let childRoute = new Route(child);

    childRoute = mutate(childRoute); // Mutate

    childRoute.calculateDistance(); // Calculate child distance

    if(childRoute.distance < firstParent.distance){ // Check if child is better than parent
        routes.push(childRoute);
    } else {
        routes.push(firstParent);
    }
};


const mutate = (individual) => {
    if (random_normal(0, 1) < MR) {
        let firstIndex = random_normal(0,individual.cities.length-1);
        let secondIndex = random_normal(0, individual.cities.length-1);

        while(firstIndex === secondIndex){
            secondIndex = random_normal(0, individual.cities.length-1);
        }

        let temp = individual.cities[firstIndex];
        individual.cities[firstIndex] = individual.cities[secondIndex];
        individual.cities[secondIndex] = temp;
    }
    return individual;
};



const GeneticsAlgorithm = () => {
    let trace = new Trace('Origin points','markers', 'scatter', generateCities(numberOfCities), {size: 15});
    const startCity = cityArray[0];

    // let startFinishTrace = new Trace('Start and finish','markers', 'scatter', [startCity,endCity], {size: 15});
    let startFinishTrace = new Trace('Start and finish','markers', 'scatter', [startCity], {size: 15});

    render(trace);

    const newCityArr = [...cityArray];

    shuffleArray(newCityArr,numberOfGeneratedRoutes, routes, startCity, null);

    console.log(routes);
    calculateFitness();

    for(let i = 0; i < GENERATIONS;i++){
        const bestRouteBackup = routes[0];

        for(let i = 0; i < routes.length; i++) {
            // Selection
            let [firstParent, secondParent] = selection(i);

            // Crossover
            crossover(firstParent, secondParent);
        }
        calculateFitness();

        if(bestRouteBackup !== routes[0]) {
            const [x,y] = routes[0].getCoordinatesForGraph();
            animateRoutes(x,y);
        }
    }
    // calculateFitness();

    console.log("Final routes",routes);
    const [x,y] = routes[0].getCoordinatesForGraph();
    // alert("Here comes the result");
    animateRoutes(x,y);
    render(trace);
    render(startFinishTrace);
};


const createDistanceMatrix = () => {
    for(let i = 0; i < numberOfCities; i++){
        for(let j = 0; j < numberOfCities;j++){
            if(i === j) {
                distancesMatrix[i][j] = 0;
            } else {
                distancesMatrix[i][j] = cityArray[i].countDistance(cityArray[j]);
            }
        }
    }
    console.log(distancesMatrix);
};

const generateCities = (n) => {
    const renderCityArray = [];

    for(let i = 0; i < n; i++){
        let x = Math.floor(Math.random() * PointsRange);
        let y = Math.floor(Math.random() * PointsRange);

        let city = new City(x,y,i);
        cityArray.push(city);
        renderCityArray.push(city);
    }
        console.log(cityArray);


    createDistanceMatrix();
    return renderCityArray;
};


/**
 * RENDER
 */
function render(trace) {
    let data = [trace.renderTrace()];

    let layout = {
        title: 'Genetics traveling salesman problem',
        width: 1200,
        height: 800,
        xaxis: {
            // range: [0, 100],
            autorange: true,
            showgrid: false,
            zeroline: false,
            showline: false,
            // showticklabels: false
        },
        yaxis: {
            autorange: true,
            showgrid: false,
            zeroline: false,
            showline: false,
            // showticklabels: false
        }
    };

    // render
    Plotly.plot(CANVAS, data, layout);
}

function animateRoutes(x,y) {
    Plotly.animate(CANVAS, {
        data: [{
            name: "Genetics algorithm",
            opacity: 1,
            mode: "markers+lines",
            markers: {size: 15},
            x: x,
            y: y,
        }]
    }, {
        transition: {
            duration: 500,
            easing: 'cubic-in',
            redraw: false
        }
    });
}

/**
 * TURN ON THE ALGORITHM
 */
GeneticsAlgorithm();


/* Current Plotly.js version */
console.log( Plotly.BUILD );
document.querySelector('#but').addEventListener("click",GeneticsAlgorithm);
