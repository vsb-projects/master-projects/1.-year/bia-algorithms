import {GriewankFunc, SphereFunc} from "./utils/functions";
import {animate, generateGraphFromFunction, getData, GraphType, render} from "./utils/renderGraph";
import {ParticlePoint, Point3D} from "./models/Point";
import {randomFromInterval} from "./utils/utilFunctions";


export function random_normal(min, max, skew = 1) {
    let u = 0, v = 0;
    while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
    while(v === 0) v = Math.random();
    let num = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );

    num = num / 10.0 + 0.5; // Translate to 0 -> 1
    if (num > 1 || num < 0) num = random_normal(min, max, skew); // resample between 0 and 1 if out of range
    num = Math.pow(num, skew); // Skew
    num *= max - min; // Stretch to fill range
    num += min; // offset to min
    return Math.round(num);
}

const sortPoints = () => {
    return particlePoints.sort((a,b) => {
        if ( a.z < b.z ){
            return -1;
        }
        if ( a.z > b.z ){
            return 1;
        }
        return 0;
    });
};


// const calculateMaxVelocity = () => {
//     let vel = range - (-range);
//     return vel / 20;
// };


/**
 * Particle swarm algorithm
 */
const ITERATIONS = 50;
// const range = 20;
const range = 5;
const particles= 20;

const maxSpeed = (range - (-range)) / 20;

const c1 = 2;
const c2 = 2;

let gBest = new ParticlePoint(0.0, 0.0, 1000.0);

let CHANGED = false;

// Searching particlePoints
const particlePoints = [];

const calculateSpeedAndAdjustPosition = (particlePoint) => {

  let speedX = particlePoint.speed.x + c1 * Math.random() * (particlePoint.pBest.x - particlePoint.x) + c2 * Math.random() * (gBest.x - particlePoint.pBest.x);
  if(speedX > maxSpeed){
      speedX = maxSpeed;
  }
  if (speedX < -maxSpeed){
      speedX = -maxSpeed;
  }

  let speedY = particlePoint.speed.y + c1 * Math.random() * (particlePoint.pBest.y - particlePoint.y) + c2 * Math.random() * (gBest.y - particlePoint.pBest.y);
  if(speedY > maxSpeed){
      speedY = maxSpeed;
  }
  if (speedY < -maxSpeed){
      speedY = -maxSpeed;
  }

  particlePoint.speed.x = speedX;
  particlePoint.speed.y = speedY;


    let nextPositionX = particlePoint.x + speedX;
    if(nextPositionX > range){
        nextPositionX = range;
    }
    if(nextPositionX < -range){
        nextPositionX = -range;
    }

    let nextPositionY = particlePoint.y + speedY;
    if(nextPositionY > range){
        nextPositionY = range;
    }
    if(nextPositionY < -range) {
        nextPositionY = -range;
    }

    particlePoint.x = nextPositionX;
    particlePoint.y = nextPositionY;

    return particlePoint;
};


const ParticleSwarm = (func) => {
    CHANGED = false;

    // Generate particlePoints
    for(let i = 0; i < particles;i++){
        let x = randomFromInterval(-range, range);
        let y = randomFromInterval(-range, range);
        let point = new ParticlePoint(x,y, func([x,y]));

        point.speed.x = randomFromInterval(-range, range) / 20;
        point.speed.y = randomFromInterval(-range, range) / 20;

        // point.speed.x = 0.0;
        // point.speed.y = 0.0;

        particlePoints.push(point);
    }

    console.log("Init particlePoints", particlePoints);

    sortPoints();

    // animate(particlePoints);

    let actualParticle = null;

    for(let i = 0; i < ITERATIONS; i++) {

        for(let j = 0; j < particles; j++){
            actualParticle = particlePoints[j];

            actualParticle = calculateSpeedAndAdjustPosition(actualParticle);

            actualParticle.z = func([actualParticle.x, actualParticle.y]);

            if(actualParticle.z < actualParticle.pBest.z) {
                actualParticle.updatepBest();
            }

            if(actualParticle.z < gBest.z){
                gBest = actualParticle;
            }
        }

        sortPoints();

        animate(particlePoints);
    }

    sortPoints();

    console.log(particlePoints);
    console.log("GLOBAL MINIMUM", particlePoints[0].z);
};

const START = (func, graphType) => {
    render(getData(generateGraphFromFunction(range, func), graphType));
    ParticleSwarm(func);
};

START(GriewankFunc, GraphType.MESH);
// START(SphereFunc, GraphType.SCATTER);