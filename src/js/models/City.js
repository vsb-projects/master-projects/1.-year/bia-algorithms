// Individual
export default class City {
    constructor(x,y,id){
        this.id = id;
        this.x = x;
        this.y = y;
        this.fitness = 0;
        this.distances = [];
    }

    countDistance(city) {
        let xDis = Math.abs(this.x - city.x);
        let yDis = Math.abs(this.y - city.y);

        return Math.sqrt((Math.pow(xDis,2) + Math.pow(yDis,2)));
    }
}
