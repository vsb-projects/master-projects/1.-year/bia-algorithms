
export default class Trace{

    constructor(name, mode, type, points = [], markers = {}){
        this.name = name;
        this.mode = mode;
        this.type = type;
        this.points = points;
        this.markers = markers;
    }

    addPoint(point){
        this.points.push(point);
    }

    renderTrace(){
        let x = [];
        let y = [];
        this.points.forEach(point => {
           x.push(point.x);
           y.push(point.y);
        });

        return ({
            name: this.name,
            x: x,
            y: y,
            mode: this.mode,
            type: this.type,
            marker: this.markers
        });
    }
}