
export class Point{
    constructor(x,y){
        this.x = x;
        this.y = y;
        this.fitness = 0.0;
    }
}

export class Point3D{
    constructor(x,y,z = 0.0){
        this.x = x;
        this.y = y;
        this.z = z;
        this.fitness = 0.0;
    }

    checkSame(anotherPoint) {
        if(this.x === anotherPoint.x){
            if(this.y === anotherPoint.y){
                if(this.z === anotherPoint.z){
                    return true;
                }
            }
        } else {
            return false;
        }
    }

    valuateSelf(func) {
        this.z = func([this.x, this.y]);
    }
}

export const add3DPoints = (point1, point2) => {
  return new Point3D(point1.x+point2.x, point1.y+point2.y);
};


export class ParticlePoint{
    constructor(x,y,z = 0.0){
        this.x = x;
        this.y = y;
        this.z = z;
        this.pBest = {
          x: 0.0,
          y: 0.0,
            z: 1000.0
        };
        this.speed = {
            x: 0.0,
            y: 0.0
        };
    }

    checkSame(anotherPoint) {
        if(this.x === anotherPoint.x){
            if(this.y === anotherPoint.y){
                if(this.z === anotherPoint.z){
                    return true;
                }
            }
        } else {
            return false;
        }
    }

    updatepBest() {
        this.pBest = {
            x: this.x,
            y: this.y,
            z: this.z
        };
    }

    valuateSelf(func) {
        this.z = func([this.x, this.y]);
    }
}

export class DimensionalPoint {
    constructor(d = [],z){
        this.d = d;
        this.x = d[0];
        this.y = d[1];
        this.z = z;
    }

    valuateSelf(func) {
        this.z = func(this.d);
        this.x = this.d[0];
        this.y = this.d[1];
    }
}