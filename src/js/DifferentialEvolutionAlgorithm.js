import {Point3D, add3DPoints} from "./models/Point";
import {animate, generateGraphFromFunction, getData, GraphType, render} from "./utils/renderGraph";
import { SphereFunc, GriewankFunc } from "./utils/functions";

const CANVAS = document.querySelector("#canvas3D");
let input = document.getElementById('in');

export function random_normal(min, max, skew = 1) {
    let u = 0, v = 0;
    while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
    while(v === 0) v = Math.random();
    let num = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );

    num = num / 10.0 + 0.5; // Translate to 0 -> 1
    if (num > 1 || num < 0) num = random_normal(min, max, skew); // resample between 0 and 1 if out of range
    num = Math.pow(num, skew); // Skew
    num *= max - min; // Stretch to fill range
    num += min; // offset to min
    return num;
}

function randomFromInterval(min, max) {
    return Math.random() * (max - min + 1) + min;
}

const sortPoints = () => {
    return points.sort((a,b) => {
        if ( a.z < b.z ){
            return -1;
        }
        if ( a.z > b.z ){
            return 1;
        }
        return 0;
    });
};



/**
 * Differential evolution Algorithm
 */
const GENERATIONS = 20;
let numberOfPoint= 30;
let range = 20.0;

const CR = 0.5;
const F = 0.8;

let CHANGED = false;

// Searching points
const points = [];

const getRandomIndexFromArray = (parentIndex, array) => {
    let otherIndex = Math.round(random_normal(0,points.length-1));
    while(parentIndex === otherIndex || array.includes(otherIndex)){
        otherIndex = Math.round(random_normal(0,points.length-1));
    }
    return otherIndex;
};

const mutation = (parentIndex) => {
    let selectedPoints = [];
    for(let i = 0; i < 3;i++) {
        selectedPoints.push(getRandomIndexFromArray(parentIndex, selectedPoints));
    }

    const [firstPointIndex, secondPointIndex, thirdPointIndex] = selectedPoints;

    let firstPoint = points[firstPointIndex];
    let secondPoint = points[secondPointIndex];
    let thirdPoint = points[thirdPointIndex];

    let mutatedPointX = firstPoint.x + F * (secondPoint.x - thirdPoint.x);
    let mutatedPointY = firstPoint.y + F * (secondPoint.y - thirdPoint.y);

    const mutatedPoint = new Point3D(mutatedPointX, mutatedPointY);

    return mutatedPoint;
};

const crossover = (parentIndex, mutant, func) => {
    const descendant = new Point3D(0,0);

    const parent = points[parentIndex];

    if(Math.round(random_normal(0,1)) === 0){
        descendant.x = mutant.x;

        if(Math.random() < CR) {
            descendant.y = mutant.y;
        } else {
            descendant.y = parent.y;
        }
    } else {
        descendant.y = mutant.y;

        if (Math.random() < CR) {
            descendant.x = mutant.x;
        } else {
            descendant.x = parent.x;
        }
    }

    descendant.z = func([descendant.x, descendant.y]);

    if(descendant.z < parent.z) {
        points[parentIndex] = descendant;
        CHANGED = true;
    }
};

const DifferentialEvolution = (func) => {
    CHANGED = false;

    // Generate points
    for(let i = 0; i < numberOfPoint;i++){
        let x = randomFromInterval(-range, range);
        let y = randomFromInterval(-range, range);
        let point = new Point3D(x,y, func([x,y]));

        points.push(point);
    }

    for(let i = 0; i < GENERATIONS; i++) {
        for (let j = 0; j < numberOfPoint; j++) {
            crossover(j, mutation(j), func);
        }

        sortPoints();

        if(CHANGED){
            CHANGED = false;
            animate(points);
        }
    }

    sortPoints();

    console.log(points);
    console.log("GLOBAL MINIMUM", points[0].z)
};


/**
 * RENDER
 */
const START = (func, graphType) => {
    render(getData(generateGraphFromFunction(range, func), graphType));
    DifferentialEvolution(func);
};

START(SphereFunc, GraphType.SCATTER);
