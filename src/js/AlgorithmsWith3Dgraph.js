import Plotly from 'plotly.js-dist';
import { Point3D } from "./models/Point";

const CANVAS_3D = document.querySelector("#canvas3D");


 let x=[]; let y=[]; let z=[];
 let pointsArray3D = [];

 const renderPoints = () => {
     const x = [];
     const y = [];
     const z = [];

     pointsArray3D.map(p =>{
         x.push(p.x);
         y.push(p.y);
         z.push(p.z);
     });

     return ({
         opacity:0.8,
         mode: "markers",
         // type: 'mesh3d',
         type: 'scatter3d',
         x: x,
         y: y,
         z: z,
         intensity: [0, 0.33, 0.66, 1],
         colorscale: [
             [0, 'rgb(255, 0, 0)'],
             [0.5, 'rgb(0, 255, 0)'],
             [1, 'rgb(0, 0, 255)']
         ],
         scene: "scene",
         name: "Sphere Function"
     });
 };

 const bumpsFunc = (x,y) => {
     return Math.sin(5 * x)*Math.cos(5 * y)/5;
 };

 const tubeFunc = (x,y) => {
     return 1/(15*(x^2+y^2))
 };

 const rippleFunc = (x,y) => {
   return Math.sin(10 * (x^2+y^2))/10;
 };

 const sphereFunc = (x) => {
     let result = 0.0;
     for(let i of x){
         result += Math.pow(i,2);
     }
     return result;
 };

 const SchwefelFunc = (x) => {
     let result = 0.0;
     for(let i of x){
         result += i * Math.sin(Math.sqrt(Math.abs(i)));
     }

     return (418.9829 - result);
 };

 const N_points = 20;

const generateGraphFromFunction = () => {

    for(let i = -N_points; i < N_points; i++){
        for(let j = -N_points; j < N_points;j++){
        x.push(i);
        y.push(j);
        // z.push(rippleFunc(i*0.1,j*0.1));
        z.push(sphereFunc([i,j]));
        // z.push(SchwefelFunc([i*0.1,j*0.1]));

        // pointsArray3D.push(new Point3D(i*0.1,j*0.1,bumpsFunc(i*0.1,j*0.1)));
        pointsArray3D.push(new Point3D(i,j,sphereFunc([i,j])));
        // pointsArray3D.push(new Point3D(i,j,sphereFunc([i,j])));
        // console.log(x,y,z);
        }
    }
};

generateGraphFromFunction();

const maxZPointOfFunction = Math.max(...pointsArray3D.map(p => p.z));

let layout_3d = {
    margin: {t:0, l:0, b:0},
    opacity: .5,
    width: 1000,
    height: 1000,
    scene: {
        // aspectratio: {
        //     x: 1, y: 0.7, z: 1,
        // },
        // xaxis: {
        //     range: [-2, 2],
        // },
        // yaxis: {
        //     range: [-2, 2],
        // },
        // zaxis: {
        //     range: [-1, 2],
        // },
        camera: {
            eye: {
                x: 1.88,
                y: -2.12,
                z: 0.96
            }
        }
    }
};


function randomFromInterval(min, max) {
    return Math.random() * (max - min + 1) + min;
}

function random_normal(min, max, skew = 1) {
    let u = 0, v = 0;
    while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
    while(v === 0) v = Math.random();
    let num = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );

    num = num / 10.0 + 0.5; // Translate to 0 -> 1
    if (num > 1 || num < 0) num = random_normal(min, max, skew); // resample between 0 and 1 if out of range
    num = Math.pow(num, skew); // Skew
    num *= max - min; // Stretch to fill range
    num += min; // offset to min
    return num;
}


const ITERATIONS = 100;

/**
 * Blind Climb Algorithm
 */
const finalMaxArray = [];
const finalMinArray = [];
let globalMin = 10;
let globalMax = 0;

let globalMinIndex_X = 0;
let globalMinIndex_Y = 0;
let globalMaxIndex_X = 0;
let globalMaxIndex_Y = 0;

let globalMinIndexes_X = [];
let globalMinIndexes_Y = [];

const BlindAlgorithm = (N_iterations) => {

    for(let i = 0; i < N_iterations;i++){
        let argX = random_normal(-N_points,N_points);
        let argY = random_normal(-N_points,N_points);

        // console.log("Rand value: " + arg,"Func value: " + sphereFunc([arg,arg]));

        if(finalMaxArray.length === 0){
            finalMaxArray.push(sphereFunc([argX, argY]));
            finalMinArray.push(sphereFunc([argX,argY]));
        } else {

            let res = sphereFunc([argX,argY]);

            if(finalMaxArray[finalMaxArray.length-1] < res){
                finalMaxArray.push(res);
            }
            if(finalMinArray[finalMinArray.length-1] > res){
                finalMinArray.push(res);
            }

            if(globalMax < res){
                globalMax = res;
                globalMaxIndex_X = argX;
                globalMaxIndex_Y = argY;
            }
            if(globalMin > res){
                globalMin = res;
                globalMinIndex_X = argX;
                globalMinIndex_Y = argY;
                globalMinIndexes_X.push(argX);
                globalMinIndexes_Y.push(argY);
            }
        }
    }

    console.log("Final Max array", finalMaxArray);
    console.log("Final Min array", finalMinArray);
    console.log("Global maximum", globalMax);
    console.log("Global minimum", globalMin);
};

BlindAlgorithm(ITERATIONS);

// Display in graph
const blindGlobalMinPoints = {
    opacity:1,
    mode: "lines+markers",
    // type: 'mesh3d',
    color:'rgb(033,255,100)',
    type: 'scatter3d',
    x: globalMinIndexes_X,
    y: globalMinIndexes_Y,
    z: finalMinArray,
    scene: "scene",
    name: 'Blind Algorithm',
};

const blindGlobalMaxPoints = {
    opacity:1,
    mode: "markers",
    // type: 'mesh3d',
    color:'rgb(033,255,100)',
    type: 'scatter3d',
    x: [globalMaxIndex_X],
    y: [globalMaxIndex_Y],
    z: [globalMax]
};


/**
 * Hill Climb Algorithm
 */
const dispersion = 3;

const finalMaxArray_Hill = [];
const finalMinArray_Hill = [];
let globalMin_Hill = 10;
let globalMax_Hill = 0;

let globalMinIndex_X_Hill = 0;
let globalMinIndex_Y_Hill = 0;
let globalMaxIndex_Hill = 0;

let globalMinIndexes_X_Hill = [];
let globalMinIndexes_Y_Hill = [];

const HILL_POINTS_GEN = 10;

const HillClimbAlgorithm = (N_iterations) => {
    /**
     * Generate random start index
     */
    globalMinIndex_X_Hill = random_normal(-N_points,N_points);
    globalMinIndex_Y_Hill = random_normal(-N_points,N_points);

    for(let i = 0; i < N_iterations; i++){
        for(let j = 0; j < HILL_POINTS_GEN; j++) {
            let argIndex_X = random_normal(-dispersion, dispersion); // Generate random number X from dispersion
            let argIndex_Y = random_normal(-dispersion, dispersion); // Generate random number Y from dispersion

            let argMinIndex_X = argIndex_X + globalMinIndex_X_Hill; // Add number to actual min X index
            let argMinIndex_Y = argIndex_Y + globalMinIndex_Y_Hill; // Add number to actual min Y index

            let resMinArg = sphereFunc([argMinIndex_X, argMinIndex_Y]);
            let resMinIndex = sphereFunc([globalMinIndex_X_Hill, globalMinIndex_Y_Hill]);

            if (resMinArg <= resMinIndex) {
                globalMin_Hill = resMinArg;
                globalMinIndex_X_Hill = argMinIndex_X;
                globalMinIndex_Y_Hill = argMinIndex_Y;

                globalMinIndexes_X_Hill.push(argMinIndex_X);
                globalMinIndexes_Y_Hill.push(argMinIndex_Y);
                finalMinArray_Hill.push(resMinArg);
            }
        }
    }

    console.log("Hill Climb final Max array", finalMaxArray_Hill);
    console.log("Hill Climb Global maximum", globalMax_Hill);
    console.log("Hill Climb final Min array", finalMinArray_Hill);
    console.log("Hill Climb Global minimum", globalMin_Hill);
};

HillClimbAlgorithm(ITERATIONS);

//Display in graph
const hillClimbGlobalMinPoints = {
    opacity:1,
    mode: "lines+markers",
    // type: 'mesh3d',
    color:'rgb(033,255,100)',
    type: 'scatter3d',
    x: globalMinIndexes_X_Hill,
    y: globalMinIndexes_Y_Hill,
    z: finalMinArray_Hill,
    scene: "scene",
    name: 'Hill Climb Algorithm'
};

const hillClimbGlobalMaxPoints = {
    opacity:1,
    mode: "markers",
    // type: 'mesh3d',
    color:'rgb(033,255,100)',
    type: 'scatter3d',
    x: [globalMaxIndex_Hill],
    y: [globalMaxIndex_Hill],
    z: [globalMax_Hill]
};

let data_3d = [
    {
        opacity:0,
        mode: "markers",
        // type: 'mesh3d',
        color:'rgb(033,255,100)',
        type: 'scatter3d',
        x: [0],
        y: [0],
        z: [0]
    },
    renderPoints(),
    blindGlobalMinPoints,
    // blindGlobalMaxPoints,
    hillClimbGlobalMinPoints,
    // hillClimbGlobalMaxPoints
];

Plotly.newPlot(CANVAS_3D, data_3d, layout_3d, {showSendToCloud: true});

const dispersion_Annealing = 3;

let T = 100.0;
const Tmin = 0.01;
const alpha = 0.99;

const Nt = 10;

const finalMaxArray_Annealing = [];
let finalMinArray_Annealing = [];
let globalMin_Annealing = 10;
let globalMax_Annealing = 0;

let globalMinIndex_X_Annealing = 0;
let globalMinIndex_Y_Annealing = 0;
let globalMaxIndex_Annealing = 0;

let globalMinIndexes_X_Annealing = [];
let globalMinIndexes_Y_Annealing = [];

/**
 * Simulated Annealing algorithm
 */
const SimulatedAnnealingAlgorithm = () => {
    /**
     * Generate random start index
     */
    let funcMin = -N_points;
    let funcMax = N_points;
    let px = pointsArray3D.map(p => p.x);
    let py = pointsArray3D.map(p => p.y);

    globalMinIndex_X_Annealing = px[Math.floor(Math.random() * px.length)];
    globalMinIndex_Y_Annealing = py[Math.floor(Math.random() * py.length)];
    // globalMinIndex_X_Annealing = random_normal(-N_points,N_points);
    // globalMinIndex_Y_Annealing = random_normal(-N_points,N_points);
    globalMinIndexes_X_Annealing.push(globalMinIndex_X_Annealing);
    globalMinIndexes_Y_Annealing.push(globalMinIndex_Y_Annealing);

    let argResMin = sphereFunc([globalMinIndex_X_Annealing, globalMinIndex_Y_Annealing]);
    globalMin_Annealing = argResMin;

    let argIndex_X = 0.0;
    let argIndex_Y = 0.0;
    let argMinIndex_X = 0.0;
    let argMinIndex_Y = 0.0;

    let changeHappened = false;

    setInterval(function(){
    if(T > Tmin) {
    // while(T > Tmin) {

        for(let i = 0; i < Nt; i++) {
            argIndex_X = random_normal(-dispersion_Annealing, dispersion_Annealing);
            argIndex_Y = random_normal(-dispersion_Annealing, dispersion_Annealing);

            argMinIndex_X = argIndex_X + globalMinIndex_X_Annealing;
            argMinIndex_Y = argIndex_Y + globalMinIndex_Y_Annealing;

            if(argMinIndex_X > funcMax){
                argMinIndex_X = funcMax;
            } else if (argMinIndex_X < funcMin){
                argMinIndex_X = funcMin;
            }

            if(argMinIndex_Y > funcMax){
                argMinIndex_Y = funcMax;
            } else if (argMinIndex_Y < funcMin){
                argMinIndex_Y = funcMin;
            }

            argResMin = sphereFunc([argMinIndex_X, argMinIndex_Y]);
            /**
             * Slide lecture implementation
             */
            let dif = argResMin - globalMin_Annealing;

            if(dif < 0){
                if(argResMin <= maxZPointOfFunction) {
                globalMin_Annealing = argResMin;
                globalMinIndex_X_Annealing = argMinIndex_X;
                globalMinIndex_Y_Annealing = argMinIndex_Y;

                    // finalMinArray_Annealing.push(argResMin);
                    // globalMinIndexes_X_Annealing.push(argMinIndex_X);
                    // globalMinIndexes_Y_Annealing.push(argMinIndex_Y);
                changeHappened = true;
                }
            } else {
                    let R = Math.pow(Math.E, -(dif) / T);
                    let a = random_normal(0.0, 1.0);
                    if (a < R) {
                        if(argResMin <= maxZPointOfFunction) {
                        globalMin_Annealing = argResMin;
                        globalMinIndex_X_Annealing = argMinIndex_X;
                        globalMinIndex_Y_Annealing = argMinIndex_Y;

                            // finalMinArray_Annealing.push(argResMin);
                            // globalMinIndexes_X_Annealing.push(argMinIndex_X);
                            // globalMinIndexes_Y_Annealing.push(argMinIndex_Y);
                        changeHappened = true;
                        }

                    }
            }
            /**
             * Other lecture implementation
             */
            // if (argResMin < globalMin_Annealing) {
            //         if(argResMin <= maxZPointOfFunction) {
            //             globalMin_Annealing = argResMin;
            //             globalMinIndex_X_Annealing = argMinIndex_X;
            //             globalMinIndex_Y_Annealing = argMinIndex_Y;
            //             // finalMinArray_Annealing.push(argResMin);
            //             // globalMinIndexes_X_Annealing.push(argMinIndex_X);
            //             // globalMinIndexes_Y_Annealing.push(argMinIndex_Y);
            //             changeHappened = true;
            //         }
            // } else {
            //     let dif = argResMin - globalMin_Annealing;
            //     let R = Math.pow(Math.E, -(dif) / T);
            //     let a = random_normal(0.0, 1.0);
            //     if (a < R) {
            //         if(argResMin <= maxZPointOfFunction) {
            //         globalMin_Annealing = argResMin;
            //         globalMinIndex_X_Annealing = argMinIndex_X;
            //         globalMinIndex_Y_Annealing = argMinIndex_Y;
            //
            //             // finalMinArray_Annealing.push(argResMin);
            //             // globalMinIndexes_X_Annealing.push(argMinIndex_X);
            //             // globalMinIndexes_Y_Annealing.push(argMinIndex_Y);
            //         changeHappened = true;
            //         }
            //     }
            // }
        }
            if(changeHappened) {
                finalMinArray_Annealing.push(globalMin_Annealing);
                globalMinIndexes_X_Annealing.push(globalMinIndex_X_Annealing);
                globalMinIndexes_Y_Annealing.push(globalMinIndex_Y_Annealing);
                // animateAnnealing();
                changeHappened = false;
                // finalMinArray_Annealing = [];
                // globalMinIndexes_X_Annealing = [];
                // globalMinIndexes_Y_Annealing = [];
            }

        T *= alpha;
    }
    },100);

    // console.log("Simulated Annealing final Max array", finalMaxArray_Annealing);
    // console.log("Simulated Annealing Global maximum", globalMax_Annealing);
    // console.log("Simulated Annealing final Min array", finalMinArray_Annealing);
    // console.log("Simulated Annealing Global minimum", globalMin_Annealing);
};


function animateAnnealing() {
    Plotly.animate(CANVAS_3D, {
        data: [{
            opacity: 1,
            mode: "markers",
            markers: {size: 2},
            // color:'rgb(033,255,100)',
            // type: 'scatter3d',
            x: globalMinIndexes_X_Annealing,
            y: globalMinIndexes_Y_Annealing,
            z: finalMinArray_Annealing
            // x: [globalMinIndex_X_Annealing],
            // y: [globalMinIndex_Y_Annealing],
            // z: [globalMin_Annealing]
        }]
    }, {
        transition: {
            duration: 10,
            // easing: 'cubic-in'
            // redraw: false
        }
    });

    // requestAnimationFrame(animateAnnealing);
}

// requestAnimationFrame(animateAnnealing);


SimulatedAnnealingAlgorithm();

