import {GriewankFunc, SphereFunc} from "./utils/functions";
import {animate, generateGraphFromFunction, getData, GraphType, render} from "./utils/renderGraph";
import {DimensionalPoint} from "./models/Point";
import {randomFromInterval} from "./utils/utilFunctions";


export function random_normal(min, max, skew = 1) {
    let u = 0, v = 0;
    while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
    while(v === 0) v = Math.random();
    let num = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );

    num = num / 10.0 + 0.5; // Translate to 0 -> 1
    if (num > 1 || num < 0) num = random_normal(min, max, skew); // resample between 0 and 1 if out of range
    num = Math.pow(num, skew); // Skew
    num *= max - min; // Stretch to fill range
    num += min; // offset to min
    return Math.round(num);
}


const sortPoints = () => {
    return tlboPoints.sort((a,b) => {
        if ( a.z < b.z ){
            return -1;
        }
        if ( a.z > b.z ){
            return 1;
        }
        return 0;
    });
};

const calculateXMean = (func) => {
    let sumArr = Array(DIMENSIONS).fill(0.0);
    for(let i = 0; i < population; i++){
        for(let j = 0; j < DIMENSIONS; j++){
            sumArr[j] += tlboPoints[i].d[j];
        }
    }
    for(let p = 0; p < sumArr.length;p++){
        sumArr[p] = sumArr[p]/population;
    }
    XMean = new DimensionalPoint(sumArr, func(sumArr));
    return XMean;
};

const TF = () => {
  return Math.round(1 + Math.random());
};

const calculateTeacherPhase = (learner, func) => {
  let newPos = Array(DIMENSIONS).fill(0.0);

  for(let i = 0; i < DIMENSIONS; i++){
      newPos[i] = learner.d[i] + Math.random() * (Teacher.d[i] - TF() + XMean.d[i]);
      if(newPos[i] < (-range)){
          newPos[i] = -range;
      }
      if (newPos[i] > range){
          newPos[i] = range;
      }
  }
  let newFitness = func(newPos);

  if(newFitness < learner.z){
      learner = new DimensionalPoint(newPos, newFitness);
  }
  return learner;
};

const calculateLearnerPhase = (learner, func) => {
    let randomLearner = null;
    do {
        randomLearner = tlboPoints[random_normal(0, population-1)];
    } while (randomLearner === learner);

    let newPos = Array(DIMENSIONS).fill(0.0);
    if(randomLearner.z <= learner.z){
        for(let i = 0; i < DIMENSIONS; i++){
            newPos[i] = learner.d[i] + Math.random() * (learner.d[i] - randomLearner.d[i]);
            if(newPos[i] < (-range)){
                newPos[i] = -range;
            }
            if (newPos[i] > range){
                newPos[i] = range;
            }
        }
    } else {
        for(let i = 0; i < DIMENSIONS; i++){
            newPos[i] = learner.d[i] + Math.random() * (randomLearner.d[i] - learner.d[i]);
            if(newPos[i] < (-range)){
                newPos[i] = -range;
            }
            if (newPos[i] > range){
                newPos[i] = range;
            }
        }
    }

    let newFitness = func(newPos);
    if(newFitness < learner.z){
        learner = new DimensionalPoint(newPos, newFitness);
    }
    return learner;
};

/**
 * Teaching learning based optimization algorithm
 */
const ITERATIONS = 500;
const DIMENSIONS = 2;
// const range = 5.0;
const range = 20.0;
const population = 20;

// CONSTANTS


// Searching tlboPoints
const tlboPoints = [];

let Teacher = null;

let XMean = new DimensionalPoint(Array(DIMENSIONS).fill(0.0),0.0);


const TLBOAlgorithm = (func, dimensions) => {

    // Generate particlePoints
    for(let i = 0; i < population;i++){
        let pointsArray = [];
        for(let j = 0; j < dimensions; j++){
            pointsArray.push(randomFromInterval(-range, range));
        }

        tlboPoints.push(new DimensionalPoint(pointsArray, func(pointsArray)));
    }

    console.log("Init tlboPoints", tlboPoints);

    sortPoints();

    animate(tlboPoints);

    for(let i = 0; i < ITERATIONS; i++) {
        Teacher = tlboPoints[0];
        calculateXMean(func);

        for(let j = 0; j < population; j++){
            // TEACHER PHASE
            tlboPoints[j] = calculateTeacherPhase(tlboPoints[j],func);

            // LEARNER PHASE
            tlboPoints[j] = calculateLearnerPhase(tlboPoints[j], func);
        }


        sortPoints();

        animate(tlboPoints);
    }

    sortPoints();

    animate(tlboPoints);

    console.log(tlboPoints);
    console.log("GLOBAL MINIMUM", tlboPoints[0].z);
};

const START = (func, graphType) => {
    render(getData(generateGraphFromFunction(range, func), graphType));
    TLBOAlgorithm(func,DIMENSIONS);
};

// START(GriewankFunc, GraphType.MESH);
START(SphereFunc, GraphType.SCATTER);