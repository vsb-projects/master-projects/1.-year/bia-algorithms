export const SphereFunc = (x) => {
    let result = 0.0;
    for(let i of x){
        result += Math.pow(i,2);
    }
    return result;
};

export const GriewankFunc = (x) => {
    let result = 0.0;

    let suma = 0.0;
    let product = 1.0;
    for(let i of x) {
        suma += ((Math.pow(i,2))/4000);
    }

    let index = 1;
    for(let j of x){
        product *= Math.cos((j/(Math.sqrt(index))));
        index++;
    }
    result = suma - product + 1;
    return result;
};