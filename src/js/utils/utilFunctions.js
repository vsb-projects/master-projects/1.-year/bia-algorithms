import {Point3D} from "../models/Point";

export const Create2DArray = (rows) => {
    let arr = [];
    for (let i = 0;i < rows;i++) {
        arr[i] = [];
    }
    return arr;
};

export const copyObject = (src) => {
    return Object.assign({},src);
};

export function randomFromInterval(min, max) {
    return Math.random() * (max - min + 1) + min;
}

export function random_normal(min, max, skew = 1) {
    let u = 0, v = 0;
    while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
    while(v === 0) v = Math.random();
    let num = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );

    num = num / 10.0 + 0.5; // Translate to 0 -> 1
    if (num > 1 || num < 0) num = random_normal(min, max, skew); // resample between 0 and 1 if out of range
    num = Math.pow(num, skew); // Skew
    num *= max - min; // Stretch to fill range
    num += min; // offset to min
    return Math.round(num);
}


export let x=[];
export let y=[];
export let z=[];
export let pointsArray3D = [];

export const renderPoints = () => {
    const x = [];
    const y = [];
    const z = [];

    pointsArray3D.map(p =>{
        x.push(p.x);
        y.push(p.y);
        z.push(p.z);
    });

    return ({
        opacity:0.8,
        mode: "markers",
        // type: 'mesh3d',
        type: 'scatter3d',
        x: x,
        y: y,
        z: z,
        intensity: [0, 0.33, 0.66, 1],
        colorscale: [
            [0, 'rgb(255, 0, 0)'],
            [0.5, 'rgb(0, 255, 0)'],
            [1, 'rgb(0, 0, 255)']
        ],
        scene: "scene",
        name: "Sphere Function"
    });
};


export const generateGraphFromFunction = (N_points) => {

    for(let i = -N_points; i < N_points; i++){
        for(let j = -N_points; j < N_points;j++){
            x.push(i);
            y.push(j);
            // z.push(rippleFunc(i*0.1,j*0.1));
            z.push(sphereFunc([i,j]));
            // z.push(SchwefelFunc([i*0.1,j*0.1]));

            // pointsArray3D.push(new Point3D(i*0.1,j*0.1,bumpsFunc(i*0.1,j*0.1)));
            pointsArray3D.push(new Point3D(i,j,sphereFunc([i,j])));
            // pointsArray3D.push(new Point3D(i,j,sphereFunc([i,j])));
            // console.log(x,y,z);
        }
    }
};
