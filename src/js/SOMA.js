import Plotly from 'plotly.js-dist';
import {Point3D, add3DPoints} from "./models/Point";
import { SphereFunc, GriewankFunc } from "./utils/functions";
import {
    generateGraphFromFunction,
    render,
    pointsArray3D,
    renderPoints,
    getData,
    animate,
    GraphType
} from "./utils/renderGraph";

const CANVAS = document.querySelector("#canvas3D");

export function random_normal(min, max, skew = 1) {
    let u = 0, v = 0;
    while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
    while(v === 0) v = Math.random();
    let num = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );

    num = num / 10.0 + 0.5; // Translate to 0 -> 1
    if (num > 1 || num < 0) num = random_normal(min, max, skew); // resample between 0 and 1 if out of range
    num = Math.pow(num, skew); // Skew
    num *= max - min; // Stretch to fill range
    num += min; // offset to min
    return Math.round(num);
}

const sortPoints = () => {
    return points.sort((a,b) => {
        if ( a.z < b.z ){
            return -1;
        }
        if ( a.z > b.z ){
            return 1;
        }
        return 0;
    });
};

/**
 * SOMA algorithm
 */
const GENERATIONS = 50;
let range = 20;
let PopSize= 50;

const PathLength = 1.1;
const Step = 0.11;

const PRT = 0.5;

const diversity = 0.001;

let CHANGED = false;

// Searching points
const points = [];


const calculatePRT = (actualVector) => {
    let PRTVector = new Point3D(0,0);

    if(random_normal(0,1) === 0){
        PRTVector.x = actualVector.x;

        if(Math.random() < PRT) {
            PRTVector.y = 1;
        } else {
            PRTVector.y = 0;
        }
    } else {
        PRTVector.y = actualVector.y;

        if (Math.random() < PRT) {
            PRTVector.x = 1;
        } else {
            PRTVector.x = 0;
        }
    }

    return PRTVector;
};

const calculateLeader = () => {
   return points.reduce((prev, current) => (prev.z < current.z) ? prev : current);
};


const calculatePathVector = (LEADER, individual) => {
    let pathVector = new Point3D(LEADER.x - individual.x, LEADER.y - individual.y);

    pathVector.x = pathVector.x * PathLength;
    pathVector.y = pathVector.y * PathLength;

    return pathVector;
};

const calculateVectorMagnitude = (vector) => {
    return Math.sqrt(Math.pow(vector.x,2) + Math.pow(vector.y,2));
};

const calculateNormalVector = (vector) => {
  let v = Math.sqrt(Math.pow(vector.x,2) + Math.pow(vector.y,2));
  let u = new Point3D(vector.x/v, vector.y/v);

  return u;
};

const getBestJumpOfJumpsArray = (jumpsArray) => {
    let bestJump = jumpsArray[0];
    for(let i = 1; i < jumpsArray.length; i++){
        if(jumpsArray[i].z < bestJump.z){
            bestJump = jumpsArray[i];
        }
    }

    return bestJump;
};

const SOMA = (func) => {
    CHANGED = false;
    let LEADER = null;
    let pointJumpsArray = [];
    let distanceToLeader = 0.0;
    let pathVector = null;
    let normalizedPathVector = null;

    let generationBest = null;

    // Generate points
    for(let i = 0; i < PopSize;i++){
        let x = random_normal(-range, range);
        let y = random_normal(-range, range);
        let point = new Point3D(x,y, func([x,y]));

        points.push(point);
    }

    console.log("Init points", points);

    sortPoints();
    generationBest = points[0];

    animate(points);

    let t = 1;
    let jump = 0;

    for(let i = 0; i < GENERATIONS; i++) {
        LEADER = calculateLeader();

        for (let j = 0; j < PopSize; j++) {
            if(LEADER.checkSame(points[j])){
                continue;
            }
            pointJumpsArray.length = 0;
            pointJumpsArray.push(points[j]);

            pathVector = calculatePathVector(LEADER, points[j]);

            distanceToLeader = calculateVectorMagnitude(pathVector);

            let PRTVector = calculatePRT(pathVector);

            normalizedPathVector = calculateNormalVector(PRTVector);

            t = 1;
            jump = t * Step;

            while(jump < distanceToLeader){
                let actualStepVector = new Point3D(normalizedPathVector.x * jump, normalizedPathVector.y * jump);

                let actualPoint = new Point3D(points[j].x + actualStepVector.x, points[j].y + actualStepVector.y);
                actualPoint.valuateSelf(func);

                pointJumpsArray.push(actualPoint);
                t++;
                jump = t * Step;
            }

            points[j] = getBestJumpOfJumpsArray(pointJumpsArray);
        }

        // Check if generation is changing
        sortPoints();

        // if(generationBest.z <= points[0].z && generationBest.z < diversity){
        //     break;
        // } else {
            generationBest = points[0];
            animate(points);
        // }
    }

    sortPoints();

    console.log(points);
};

const START = (func, graphType) => {
    render(getData(generateGraphFromFunction(range, func), graphType));
    SOMA(func);
};

// START(GriewankFunc, GraphType.MESH);
START(SphereFunc, GraphType.SCATTER);