/**
 * Permutations
 */
const displayPermutatedArray = [];
let count = 0;
function permutate(array,l,r) {
    if(l === r){
        console.log(array, count);count++;
    } else {
        for (let i = l; i <= r; i++) {
            array = swap(array, l, i);
            permutate(array, l + 1, r);
            array = swap(array, l, i);
        }
    }
}

function swap(array, i, j){
    let temp;
    temp = array[i];
    array[i] = array[j];
    array[j] = temp;
    return array;
}

const arr = [2,4,6,5,9];
// permutate(arr, 0,arr.length - 1);