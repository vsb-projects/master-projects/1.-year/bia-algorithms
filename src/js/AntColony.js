import Plotly from 'plotly.js-dist';
import Trace from "./models/Trace";
import City from "./models/City";
import { Create2DArray, random_normal } from "./utils/utilFunctions";

let CANVAS = document.querySelector('#canvas');
let input = document.getElementById('in');

class Ant {
    constructor(actualCity){
        this.actualCity = actualCity;
        this.path = new Array(actualCity);
        this.distance = 0.0;
    }

    setActualCity(city){
        this.actualCity = city;
    }

    calculateDistance(){
        this.path = this.path.filter(item => item);
        try {
            for (let i = 1; i < this.path.length; i++) {
                this.distance += distancesMatrix[this.path[i - 1].id][this.path[i].id];
            }
            this.distance += distancesMatrix[this.path[0].id][this.path[this.path.length - 1].id];
        } catch (e) {
            console.log(e);
        }
    }

    getCoordinatesForGraph() {
        let x = [];
        let y = [];

        for(let city of this.path){
            x.push(city.x);
            y.push(city.y);
        }

        x.push(this.path[0].x);
        y.push(this.path[0].y);

        return [x,y];
    }
}

const TSP_dataset = [
    [0, 13],
    [0, 26],
    [0, 27],
    [0, 39],
    [2, 0],
    [5, 13],
    [5, 19],
    [5, 25],
    [5, 31],
    [5, 37],
    [5, 43],
    [5, 8],
    [8, 0],
    [9, 10],
    [10, 10],
    [11, 10],
    [12, 10],
    [12, 5],
    [15, 13],
    [15, 19],
    [15, 25],
    [15, 31],
    [15, 37],
    [15, 43],
    [15, 8],
    [18, 11],
    [18, 13],
    [18, 15],
    [18, 17],
    [18, 19],
    [18, 21],
    [18, 23],
    [18, 25],
    [18, 27],
    [18, 29],
    [18, 31],
    [18, 33],
    [18, 35],
    [18, 37],
    [18, 39],
    [18, 41],
    [18, 42],
    [18, 44],
    [18, 45],
    [25, 11],
    [25, 15],
    [25, 22],
    [25, 23],
    [25, 24],
    [25, 26],
    [25, 28],
    [25, 29],
    [25, 9],
    [28, 16],
    [28, 20],
    [28, 28],
    [28, 30],
    [28, 34],
    [28, 40],
    [28, 43],
    [28, 47],
    [32, 26],
    [32, 31],
    [33, 15],
    [33, 26],
    [33, 29],
    [33, 31],
    [34, 15],
    [34, 26],
    [34, 29],
    [34, 31],
    [34, 38],
    [34, 41],
    [34, 5],
    [35, 17],
    [35, 31],
    [38, 16],
    [38, 20],
    [38, 30],
    [38, 34],
    [40, 22],
    [41, 23],
    [41, 32],
    [41, 34],
    [41, 35],
    [41, 36],
    [48, 22],
    [48, 27],
    [48, 6],
    [51, 45],
    [51, 47],
    [56, 25],
    [57, 12],
    [57, 25],
    [57, 44],
    [61, 45],
    [61, 47],
    [63, 6],
    [64, 22],
    [71, 11],
    [71, 13],
    [71, 16],
    [71, 45],
    [71, 47],
    [74, 12],
    [74, 16],
    [74, 20],
    [74, 24],
    [74, 29],
    [74, 35],
    [74, 39],
    [74, 6],
    [77, 21],
    [78, 10],
    [78, 32],
    [78, 35],
    [78, 39],
    [79, 10],
    [79, 33],
    [79, 37],
    [80, 10],
    [80, 41],
    [80, 5],
    [81, 17],
    [84, 20],
    [84, 24],
    [84, 29],
    [84, 34],
    [84, 38],
    [84, 6],
    [107, 27]
];



/**
 * Ant colony algorithm
 */
// If ants are generated randomly numberOfCities must be slightly bigger than numberOfAnts
const GENERATIONS = 50;
let numberOfCities = 12;

const numberOfAnts = 10;
const antPopulation = [];

const PointsRange = 200;

let bestAnt = new Ant(new City(0.0,0.0,0));
bestAnt.distance = 100000;

const alpha = 1;
const beta = 2;


// Matrixs and lists
const cityArray = [];
const renderCities = [];
let distancesMatrix = [];
let inversionMatrix = [];
let pheromoneMatrix = [];


const createPheromoneMatrix = () => {
    pheromoneMatrix = Create2DArray(numberOfCities);

  for(let i = 0; i < numberOfCities; i++){
      for (let j = 0; j < numberOfCities; j++){
          pheromoneMatrix[i][j] = 1;
      }
  }
  return pheromoneMatrix;
};

const createInversionMatrix = () => {
    inversionMatrix = Create2DArray(numberOfCities);

    for(let i = 0; i < numberOfCities; i++){
        for (let j = 0; j < numberOfCities; j++){
            inversionMatrix[i][j] = 1/distancesMatrix[i][j];
        }
    }
    return inversionMatrix;
};

const generateNewAnts = (random = true, startCityIndex = 0) => {
    if(random) {
        const arr = [];
        let chosenCity = null;
        for (let i = 0; i < numberOfAnts; i++) {
            do {
                chosenCity = cityArray[Math.round(random_normal(0, numberOfCities-1))];
            } while (chosenCity === undefined || arr.includes(chosenCity));

            let hmm = arr.includes(chosenCity);

            // chosenCity = cityArray[Math.round(random_normal(0, numberOfCities-1))];

            arr.push(chosenCity);
            antPopulation.push(new Ant(chosenCity));
        }
    } else {
        for (let i = 0; i < numberOfCities; i++) {
            antPopulation.push(new Ant(cityArray[startCityIndex]));
        }
    }
};


/**
 * ALGORITHM
 */
const AntColonyAlgorithm = () => {
    /////////////// City generation Type ///////////////
    generateCities(numberOfCities);
    // generateCitiesFromTSPdataset();
    /////////////////////////////////////////////////

    let trace = new Trace('Origin points','markers', 'scatter', renderCities, {size: 15});
    const startCity = cityArray[0];

    let startFinishTrace = new Trace('Start and finish','markers', 'scatter', [startCity], {size: 15});

    render(trace);

    // START ALGORITHM
    for(let g = 0; g < GENERATIONS;g++){
        /////////////// Ants generation Type ///////////////
        generateNewAnts();
        // generateNewAnts(false, 0);
        //////////////////////////////////////////////

        let currentAnt = null;
        for(let i = 0; i < antPopulation.length; i++) {
            currentAnt = antPopulation[i];

            const possibleCities = cityArray.filter(f => f !== currentAnt.actualCity);
            const cityProbabilities = [];
            const cumulativeProbabilities = [];

            while(possibleCities.length > 0) {
                /**
                 * Calculate probabilities
                 */
                let probabilityStep = 0.0;
                let sumaProbability = 0.0;
                for (let city of possibleCities) {
                    const pheromoneValue = pheromoneMatrix[city.id][currentAnt.actualCity.id];
                    const distanceValue = inversionMatrix[city.id][currentAnt.actualCity.id];

                    const probability = Math.pow(pheromoneValue, 2) + Math.pow(distanceValue, 2);

                    cityProbabilities.push(probability);
                    probabilityStep += probability;
                }
                for (let i = 0; i < cityProbabilities.length; i++) {
                    cityProbabilities[i] /= probabilityStep;
                    sumaProbability += cityProbabilities[i];
                }

                /**
                 * Calculate cumulative probabilities
                 */
                for (let i = 0; i < cityProbabilities.length; i++) {
                    let probCount = 0.0;
                    for (let j = 0; j <= i; j++) {
                        probCount += cityProbabilities[j];
                    }
                    let cumProb = probCount / sumaProbability;

                    cumulativeProbabilities.push(cumProb);
                }

                /**
                 * Choose next best city
                 */
                let randomNumber = Math.random();
                const closestIndex = getClosestIndex(randomNumber, cumulativeProbabilities);
                // const nearestValue = cumulativeProbabilities.reduce((prev, curr) => Math.abs(curr - randomNumber) < Math.abs(prev - randomNumber) ? curr : prev);

                currentAnt.setActualCity(possibleCities[closestIndex]);
                currentAnt.path.push(possibleCities[closestIndex]);
                possibleCities.splice(closestIndex, 1);
                cityProbabilities.length = 0;
                cumulativeProbabilities.length = 0;
            }

            /**
             * Add start city
             */

            /**
             * Calculate ant path distance
             */
            currentAnt.calculateDistance();

            if(currentAnt.distance < bestAnt.distance){
                bestAnt.distance = currentAnt.distance;
                bestAnt.path = [...currentAnt.path];
                bestAnt.actualCity = currentAnt.actualCity;
                if(g % 2 === 0) {
                    const [x, y] = bestAnt.getCoordinatesForGraph();
                    animateRoutes(x, y);
                }
            }
        }

        /**
         * Re-calculate pheromone matrix
         */
        for(let i = 0; i < numberOfCities; i++){
            for(let j = 0; j < numberOfCities; j++){
                pheromoneMatrix[i][j] *= 0.5;
            }
        }
        for(let ant of antPopulation){
            for(let i = 1; i < ant.path; i++){
                let prevIndex = ant.path[i-1];
                let curIndex = ant.path[i];
                pheromoneMatrix[prevIndex][curIndex] += (1/ant.distance);
            }
        }

        antPopulation.length = 0;
    }

    console.log("Best Ant",bestAnt);
    const [x,y] = bestAnt.getCoordinatesForGraph();
    animateRoutes(x,y);
    render(trace);
    // render(startFinishTrace);
};

function getClosestIndex (num, arr) {
    let closestIndex = 0;

    let curr = arr[0];
    let diff = Math.abs (num - curr);
    for (let val = 0; val < arr.length; val++) {
        let newdiff = Math.abs (num - arr[val]);
        if (newdiff < diff) {
            diff = newdiff;
            curr = arr[val];
            closestIndex = val;
        }
    }
    // return curr;
    return closestIndex;
}


const createDistanceMatrix = () => {
    distancesMatrix = Create2DArray(numberOfCities);

    for(let i = 0; i < numberOfCities; i++){
        for(let j = 0; j < numberOfCities;j++){
            if(i === j) {
                distancesMatrix[i][j] = 0;
            } else {
                distancesMatrix[i][j] = cityArray[i].countDistance(cityArray[j]);
            }
        }
    }
    // console.log(distancesMatrix);
};

const generateCities = (n) => {
    for(let i = 0; i < n; i++){
        let x = Math.floor(Math.random() * PointsRange);
        let y = Math.floor(Math.random() * PointsRange);

        let city = new City(x,y,i);
        cityArray.push(city);
        renderCities.push(city);
    }
    // console.log(cityArray);

    createDistanceMatrix();

    createPheromoneMatrix();
    createInversionMatrix();

    return renderCities;
};


const generateCitiesFromTSPdataset = () => {
    let index = 0;
    for(let uhu of TSP_dataset){
        let city = new City(uhu[0],uhu[1],index);
        cityArray.push(city);
        renderCities.push(city);
        index++;
    }

    numberOfCities = index;

    createDistanceMatrix();

    createPheromoneMatrix();
    createInversionMatrix();

    return renderCities;
};

/**
 * RENDER
 */
function render(trace) {
    let data = [trace.renderTrace()];

    let layout = {
        title: 'Ant Colony Optimization used for TSP',
        width: 1600,
        height: 1000,
        xaxis: {
            // range: [0, 100],
            autorange: true,
            showgrid: false,
            zeroline: false,
            showline: false,
            // showticklabels: false
        },
        yaxis: {
            autorange: true,
            showgrid: false,
            zeroline: false,
            showline: false,
            // showticklabels: false
        }
    };

    // render
    Plotly.plot(CANVAS, data, layout);
}

function animateRoutes(x,y) {
    Plotly.animate(CANVAS, {
        data: [{
            name: "ACO algorithm",
            opacity: 1,
            mode: "markers+lines",
            markers: {size: 15},
            x: x,
            y: y,
        }]
    }, {
        transition: {
            duration: 300,
            easing: 'cubic-in',
            redraw: true
        }
    });
}

/**
 * TURN ON THE ALGORITHM
 */
AntColonyAlgorithm();


/* Current Plotly.js version */
console.log( Plotly.BUILD );
document.querySelector('#but').addEventListener("click",AntColonyAlgorithm);
